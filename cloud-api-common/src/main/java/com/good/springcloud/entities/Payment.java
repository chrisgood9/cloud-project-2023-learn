package com.good.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/2 18:21
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {

    //bigInt 类型
    private Long id;

    private String serial;

}
