package com.good.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/11 17:31
 */
@RestController
@Slf4j
public class ConfigClientController {

    @Value("${config.info}")  //gitee里的yml文件里的内容
    private String configInfo;

    @Value("${server.port}")
    private String port;

    @GetMapping("/config/getInfo")
    public String getConfigInfo() {
        return "port = " + port + "\t" + configInfo;
    }

}
