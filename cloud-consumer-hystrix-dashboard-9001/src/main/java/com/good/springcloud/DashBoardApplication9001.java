package com.good.springcloud;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/10 16:46
 */
@SpringBootApplication
@EnableHystrixDashboard
public class DashBoardApplication9001 {
    public static void main(String[] args) {
        SpringApplication.run(DashBoardApplication9001.class, args);
    }


}
