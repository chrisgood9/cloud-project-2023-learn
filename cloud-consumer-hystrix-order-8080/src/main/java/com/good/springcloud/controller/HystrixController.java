package com.good.springcloud.controller;

import com.good.springcloud.service.HystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 23:31
 */

@RestController
@Slf4j
public class HystrixController {

    @Resource
    private HystrixService hystrixService;

    @GetMapping(value = "/consumer/payment/hystrix/normal/{id}")
    public String normal(@PathVariable("id") Long id) {
        return hystrixService.normal(id);
    }

    @HystrixCommand(fallbackMethod = "timeOutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500")
    })
    @GetMapping(value = "/consumer/payment/hystrix/timeout/{id}")
    public String timeOut(@PathVariable("id") Long id) {
        return hystrixService.timeOut(id);
    }

    public String timeOutHandler(@PathVariable("id") Long id) {
        return "消费端/超时处理方法-Hystrix| 线程池：" + Thread.currentThread().getName() + "\t线程id：" + id;
    }
}
