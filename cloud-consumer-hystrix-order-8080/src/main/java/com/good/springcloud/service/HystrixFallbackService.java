package com.good.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/10 15:39
 */

/**
 * 2.0版本——配置服务降级
 * 1). 创建一个类实现要降级的服务类
 * 2). 填写对应服务的降级业务
 * 3). 在对应的服务类中：注解@FeignClient 添加fallback属性,标注fallback的类
 *        @FeignClient( fallback = HystrixFallbackService.class)
 */
@Component
public class HystrixFallbackService implements HystrixService {
    @Override
    public String normal(Long id) {
        return "请稍后再试——2.0版本";
    }

    @Override
    public String timeOut(Long id) {
        return "请稍后再试——2.0版本";
    }
}
