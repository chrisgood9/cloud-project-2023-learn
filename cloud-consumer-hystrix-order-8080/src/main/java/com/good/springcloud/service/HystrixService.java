package com.good.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 23:28
 */

@Component
@FeignClient(value = "CLOUD-HYSTRIX-PAYMENT-SERVICE", fallback = HystrixFallbackService.class)
public interface HystrixService {

    @GetMapping(value = "/payment/hystrix/normal/{id}")
    public String normal(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/hystrix/timeout/{id}")
    public String timeOut(@PathVariable("id") Long id);

}
