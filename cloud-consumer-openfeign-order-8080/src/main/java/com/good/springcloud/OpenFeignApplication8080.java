package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 9:59
 */
@SpringBootApplication
@EnableFeignClients     //开启feign
public class OpenFeignApplication8080 {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignApplication8080.class, args);
    }
}
