package com.good.springcloud.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 10:44
 */

/**
 * OpenFeign开启日志打印 —— 配置日志级别
 * 四种级别：none、basic、headers、full
 * 默认是none
 */
@Configuration
public class FeignConfig {
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
