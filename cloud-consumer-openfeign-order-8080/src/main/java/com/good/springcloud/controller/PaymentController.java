package com.good.springcloud.controller;

import com.good.springcloud.entities.CommonResult;
import com.good.springcloud.entities.Payment;
import com.good.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 10:08
 */

@RestController
@Slf4j
@RequestMapping("/consumer")
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @GetMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment) {
        return paymentService.create(payment);
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        return paymentService.getPaymentById(id);
    }

    @GetMapping(value = "/payment/get/ttl")
    public CommonResult<String> openFeignOutTime() {
        //OpenFeign-ribbon: 一般默认等待一秒,超过一秒报错
        return paymentService.openFeignOutTime();
    }

}
