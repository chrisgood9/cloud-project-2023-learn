package com.good.springcloud.service;

import cn.hutool.core.thread.ThreadUtil;
import com.good.springcloud.config.FeignConfig;
import com.good.springcloud.entities.CommonResult;
import com.good.springcloud.entities.Payment;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 10:04
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE",configuration = FeignConfig.class) //绑定的服务名称
@RequestMapping("/payment")
public interface PaymentService {

    @GetMapping(value = "/create")
    public CommonResult create(@RequestBody Payment payment);

    @GetMapping(value = "/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/get/ttl")
    public CommonResult<String> openFeignOutTime();
}
