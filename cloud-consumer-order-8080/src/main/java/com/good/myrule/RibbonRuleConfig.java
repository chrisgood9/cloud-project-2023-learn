package com.good.myrule;

import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/8 10:29
 */
@Configuration
public class RibbonRuleConfig {
    @Bean
    public RandomRule ribbonRule(){
        //随机负载均衡策略
        return new RandomRule();
    }
}
