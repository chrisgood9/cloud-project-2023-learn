package com.good.springcloud;

import com.good.myrule.RibbonRuleConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 14:04
 */

/**
 * 无consul版本
 */
//这里没有使用到数据库，所以把自动配置数据源排除掉
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE",configuration = RibbonRuleConfig.class)
public class ConsumerApplication8080 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication8080.class, args);
    }
}
