package com.good.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 14:19
 */
@Configuration
public class ApplicationContextConfig {
    @Bean
//    @LoadBalanced   //开启负载均衡，可以使用服务名称来进行访问
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
