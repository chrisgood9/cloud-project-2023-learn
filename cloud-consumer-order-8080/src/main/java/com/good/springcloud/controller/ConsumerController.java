package com.good.springcloud.controller;

import com.good.springcloud.entities.CommonResult;
import com.good.springcloud.entities.Payment;
import com.good.springcloud.lb.LoadBalancer;
import com.good.springcloud.lb.MyLB;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 14:17
 */

@Slf4j
@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    //暂时使用静态的远程调用的地址
    public static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    //自定义的负载均衡算法
    @Resource
    private LoadBalancer loadBalancer;
    //服务发现
    @Resource
    private DiscoveryClient discoveryClient;

    /**
     * 远程调用 post请求，参数不需要@RequestBody注解
     * 注意：这里不需要 @RequestBody注解
     * 错误写法：
     * public CommonResult<Payment> create(@RequestBody Payment payment) {...}
     */
    @GetMapping("/payment/create")
    public CommonResult<Payment> create(Payment payment) {
        CommonResult result = restTemplate.postForObject(PAYMENT_URL + "/payment/create", payment, CommonResult.class);
        return result;
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        CommonResult result = restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
        return result;
    }

    //服务发现
    @GetMapping("/payment/discovery")
    public CommonResult<DiscoveryClient> getDiscovery() {
        CommonResult result = restTemplate.getForObject(PAYMENT_URL + "/payment/discovery", CommonResult.class);
        return result;
    }

    @GetMapping("/ribbon/payment/get/{id}")
    public CommonResult<Payment> getPaymentById2(@PathVariable("id") Long id) {
        ResponseEntity<CommonResult> entity = restTemplate.getForEntity(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
        if (entity.getStatusCode().is2xxSuccessful()) {  //判断状态码是否是 2xx
            return entity.getBody();
        } else {
            return new CommonResult<Payment>(444, "操作失败");
        }
    }

    @GetMapping("/lb/get")
    public CommonResult<String> getServerPort() {
        //根据服务名称获取服务列表
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        //调用自定义的负载均衡算法——获取服务
        ServiceInstance instance = loadBalancer.instance(instances);
        URI uri = instance.getUri();
        String result = restTemplate.getForObject(uri + "/payment/lb/get", String.class);
        return new CommonResult<>(200, result);
    }

    public void test(){
        List instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
    }

}
