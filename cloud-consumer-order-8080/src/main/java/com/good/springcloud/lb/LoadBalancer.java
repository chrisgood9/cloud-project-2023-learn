package com.good.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/8 17:01
 */

public interface LoadBalancer {

    ServiceInstance instance(List<ServiceInstance> serviceInstances);

}
