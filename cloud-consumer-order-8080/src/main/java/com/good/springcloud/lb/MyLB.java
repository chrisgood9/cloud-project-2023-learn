package com.good.springcloud.lb;

import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/8 17:04
 */

/**
 * 自定义的负载均衡算法
 *  CAS + 自旋锁
 */
@Component
public class MyLB implements LoadBalancer {

    //初始值
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    //获取并增加
    public final int getAndIncrement() {
        int current;
        int next;
        do {
            current = this.atomicInteger.get();
            next = current >= 2147483647 ? 0 : current + 1;
        } while (!this.atomicInteger.compareAndSet(current, next));
        System.out.println("访问次数：next =" + next);
        return next;
    }

    //在服务列表中获取服务
    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size();
        return serviceInstances.get(index);
    }

}
