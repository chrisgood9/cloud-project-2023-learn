package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/5 16:45
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ConsumerZKApplication8080 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerZKApplication8080.class, args);
    }
}
