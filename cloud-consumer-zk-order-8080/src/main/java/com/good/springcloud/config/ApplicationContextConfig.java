package com.good.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/5 16:51
 */
@Configuration
public class ApplicationContextConfig {

    @LoadBalanced  //开启负载均衡
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
