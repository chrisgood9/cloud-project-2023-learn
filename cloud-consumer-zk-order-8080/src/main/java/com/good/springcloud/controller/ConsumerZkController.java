package com.good.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/5 16:46
 */
@Slf4j
@RestController
@RequestMapping(value = "/consumer")
public class ConsumerZkController {
    //静态版——远程调用
    public static final String INVOKE_URL = "http://cloud-provider-payment";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "/zookeeper/print")
    public String printPaymentInfo() {
        String result = restTemplate.getForObject(INVOKE_URL + "/zookeeper/print", String.class);
        return result;
    }
}
