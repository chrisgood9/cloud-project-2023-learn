package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 16:30
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServiceApplication7001 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServiceApplication7001.class, args);
    }
}
