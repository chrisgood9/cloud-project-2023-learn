package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/4 16:09
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServiceApplication7002 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServiceApplication7002.class, args);
    }
}
