package com.good.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/10 22:38
 */

/**
 * 配置类的形式，设置gateway——路由
 */
@Configuration
public class GatewayConfig {
    /**
     * 出现跨域问题
     */
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("path_rout_good",
                r -> r.path("/guonei").uri("https://www.bilibili.com/"));
        return routes.build();
    }
}
