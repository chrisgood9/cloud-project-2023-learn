package com.good.springcloud.filter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/11 15:31
 */
@Component
@Slf4j
public class MyLogGateWayFilter implements GlobalFilter, Ordered {
    /**
     * chain.filter(exchange)是一个过滤器链，用于处理HTTP请求和响应
     * exchange对象，包含啊了http请求和响应的所有信息。
     * 过滤器链会以此执行其中的过滤器，对请求和响应进行处理和转换，
     * 最终返回处理后的 exchange对象
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("--------------MyLogGateWayFilter  Start-------------" + new Date());
        String uname = exchange.getRequest().getQueryParams().getFirst("uname");
        if (uname == null) {
            log.info("用户名为null，非法用户QAQ");
            //设置response 状态码 406
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            //完成请求调用
            return exchange.getResponse().setComplete();
        }
        log.info("--------------MyLogGateWayFilter  End-------------" + new Date());
        return chain.filter(exchange); //过滤器链 用于处理HTTP请求和响应
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
