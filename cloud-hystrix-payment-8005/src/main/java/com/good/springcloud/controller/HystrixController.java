package com.good.springcloud.controller;

import com.good.springcloud.service.HystrixService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 23:18
 */
@RestController
@Slf4j
public class HystrixController {

    @Resource
    private HystrixService hystrixService;

    @GetMapping(value = "/payment/hystrix/normal/{id}")
    public String normal(@PathVariable("id") Long id) {
        return hystrixService.normal(id);
    }

    @GetMapping(value = "/payment/hystrix/timeout/{id}")
    public String timeOut(@PathVariable("id") Long id) {
        return hystrixService.timeOut(id);
    }


    @GetMapping(value = "/payment/hystrix/global/{id}")
    public String timeOutGlobal(@PathVariable("id") Long id) {
        return hystrixService.timeOutGlobal(id);
    }

    @GetMapping(value = "/payment/hystrix/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Long id){
        return hystrixService.paymentCircuitBreaker(id);
    }
}
