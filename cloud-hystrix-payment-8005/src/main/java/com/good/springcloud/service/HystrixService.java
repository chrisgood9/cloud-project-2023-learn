package com.good.springcloud.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 23:13
 * <p>
 * 学习 Hystrix
 */
public interface HystrixService {

    String normal(@Param("id") Long id);

    String timeOut(@Param("id") Long id);

    String timeOutGlobal(@Param("id") Long id);

    public String paymentCircuitBreaker(@Param("id") Long id);
}
