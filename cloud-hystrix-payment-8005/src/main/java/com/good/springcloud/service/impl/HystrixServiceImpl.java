package com.good.springcloud.service.impl;

import cn.hutool.core.util.IdUtil;
import com.good.springcloud.service.HystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;


/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/9 23:16
 */
@Service
@DefaultProperties(defaultFallback = "payment_Global_FallBackMethod")
public class HystrixServiceImpl implements HystrixService {

    @Override
    public String normal(Long id) {
        return "正常状态-无超时 | 线程池：" + Thread.currentThread().getName() + "\t线程id：" + id;
    }

    /**
     * @HystrixCommand 服务降级配置：在里面指定超时/出错的回调方法，作为兜底方法
     * 可以在服务端声明，也可以在消费端，一般放在消费端。
     */
    @Override
    @HystrixCommand(fallbackMethod = "timeOutHandler", //超时回调方法
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
            })//超时时间
    public String timeOut(Long id) {
        int sleepTime = 5;
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Thread.currentThread().interrupt();//恢复中断标志位
        }
        return "测试超时后 | 线程池：" + Thread.currentThread().getName() + "\t线程id：" + id;
    }

    public String timeOutHandler(Long id) {
        return "服务端/超时处理方法-Hystrix| 线程池：" + Thread.currentThread().getName() + "\t线程id：" + id;
    }

    /**
     * 全局的 fallback 方法
     */
    public String payment_Global_FallBackMethod() {
        return "全局fallback方法——Global异常处理，请稍后再试~<( _ _ )>";
    }

    @Override
    @HystrixCommand
    public String timeOutGlobal(Long id) {
        int sleepTime = 5;
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Thread.currentThread().interrupt();//恢复中断标志位
        }
        return "测试超时后 | 线程池：" + Thread.currentThread().getName() + "\t线程id：" + id;
    }

    /**
     * ====服务熔断====
     */
    @Override
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(@PathVariable("id") Long id) {
        if (id < 0) {
            throw new RuntimeException("id不能为负数");
        }
        String uuid = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "\t" + "调用成功，流水号： " + uuid;
    }

    public String paymentCircuitBreaker_fallback(@PathVariable("id") Long id) {
        return "id不能为负数，请稍候再试，/(T o T)/~~ id: " + id;
    }
}
