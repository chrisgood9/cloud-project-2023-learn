package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/2 17:29
 */

/**
 * @EnableDiscoveryClient ：
 *  具体的实现是通过项目classpath来决定,
 *  因此可以为不同的注册中心提供服务的注册与发现
 *  (如:已知的eureka,Zookeeper以及consul等).
 */
@SpringBootApplication
@EnableEurekaClient //在eureka中注册服务
@EnableDiscoveryClient //在注册中心中注册服务 -
public class PaymentApplication8001 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication8001.class, args);
    }
}
