package com.good.springcloud.controller;

import cn.hutool.core.thread.ThreadUtil;
import com.good.springcloud.entities.CommonResult;
import com.good.springcloud.entities.Payment;
import com.good.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 9:29
 */
//@Api(tags = "payment接口")
@RestController
@Slf4j
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Resource
    private DiscoveryClient discoveryClient;

    @Value("${server.port}")
    private String port;

//    @ApiOperation("插入用户")
    @PostMapping("/create")
    public CommonResult create(@RequestBody Payment payment) {
        Integer result = paymentService.create(payment);
        if (result > 0) {
            return new CommonResult(200, "success——port:" + port, result);
        } else {
            return new CommonResult(444, "failure", null);
        }
    }

//    @ApiOperation("根据id查询")
    @GetMapping("/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        if (payment != null) {
            return new CommonResult(200, "success——port:" + port, payment);
        } else {
            return new CommonResult(444, "failure", null);
        }
    }

    /**
     * 服务发现
     * 对于注册进eureka里面的微服务，可以通过服务发现来获得该服务的信息。
     */
    @GetMapping(value = "/discovery")
    public DiscoveryClient getDiscovery() {
        // 获取服务列表清单
        List<String> services = discoveryClient.getServices();
        for (String regions : services) {
            log.info(regions);
        }
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        // 根据具体服务进一步获得该微服务的信息
        for (ServiceInstance instance : instances) {
            log.info(instance.getInstanceId() + "\t" + instance.getHost()
                    + "\t" + instance.getPort() + "\t" + instance.getUri());
        }
        return discoveryClient;
    }

    @GetMapping(value = "/lb/get")
    public String getPaymentIdWithLB() {
        return port;
    }

    @GetMapping(value = "/get/ttl")
    public String openFeignOutTime() {
        ThreadUtil.sleep(3000);
        return "休眠三秒钟了噢" + port;
    }
}
