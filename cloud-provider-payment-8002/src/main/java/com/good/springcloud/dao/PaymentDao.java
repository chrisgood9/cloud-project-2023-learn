package com.good.springcloud.dao;

import com.good.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 9:15
 */

/**
 * 可以在dao中声明 @Mapper注解，
 * 也可以在启动类上用 @MapperScan注解来扫描
 */
@Mapper
public interface PaymentDao {

    public Integer create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);

}
