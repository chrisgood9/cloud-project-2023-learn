package com.good.springcloud.service;

import com.good.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 9:26
 */
public interface PaymentService {

    public Integer create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);

}
