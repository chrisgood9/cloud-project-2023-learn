package com.good.springcloud.service.impl;

import com.good.springcloud.dao.PaymentDao;
import com.good.springcloud.entities.Payment;
import com.good.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/3 9:26
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public Integer create(Payment payment) {
        Integer success = paymentDao.create(payment);
        //返回结果>0 则插入成功
        return success;
    }

    @Override
    public Payment getPaymentById(Long id) {
        Payment payment = paymentDao.getPaymentById(id);
        return payment;
    }

}
