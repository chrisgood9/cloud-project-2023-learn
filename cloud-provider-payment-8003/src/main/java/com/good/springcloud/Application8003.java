package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/5 16:09
 */
@SpringBootApplication
@EnableDiscoveryClient  //注册服务
public class Application8003 {

    public static void main(String[] args) {
        SpringApplication.run(Application8003.class, args);
    }

}
