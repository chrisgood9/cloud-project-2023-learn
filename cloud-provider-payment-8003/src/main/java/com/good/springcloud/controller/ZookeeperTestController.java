package com.good.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/5 16:10
 */
@RestController
@RequestMapping(value = "/zookeeper")
@Slf4j
public class ZookeeperTestController {

    @Value("${server.port}")
    private String serverPort;

    @RequestMapping(value = "/print", method = RequestMethod.GET)
    public String printPortAndUUID() {
        return "springcloud with zookeeper: " + serverPort + "\t+uuid : " + UUID.randomUUID().toString();
    }

}
