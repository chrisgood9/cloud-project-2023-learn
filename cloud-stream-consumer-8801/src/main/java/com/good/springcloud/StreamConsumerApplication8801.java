package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 16:05
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class StreamConsumerApplication8801 {
    public static void main(String[] args) {
        SpringApplication.run(StreamConsumerApplication8801.class, args);
    }
}
