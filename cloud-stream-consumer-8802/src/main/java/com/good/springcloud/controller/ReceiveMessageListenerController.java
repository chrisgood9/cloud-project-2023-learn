package com.good.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 16:06
 */
@Component
@EnableBinding(Sink.class) //定义推送管道 sink
@Slf4j
public class ReceiveMessageListenerController {

    @Value("${server.port}")
    private String serverPort;

    /**
     * 接收消息 getPayload()
     * 只要在同一个 exchange ，就可以接收到 output发送的消息
     **/
    @StreamListener(Sink.INPUT) // 输入源 Sink.INPUT
    public void input(Message<String> message) {
        log.info("消费者2 ___接收的消息：{}", message.getPayload());

    }
}
