package com.good.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 15:01
 */

/**
 * 消息驱动 stream 生产者
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class StreamApplication8001 {
    public static void main(String[] args) {
        SpringApplication.run(StreamApplication8001.class, args);
    }
}
