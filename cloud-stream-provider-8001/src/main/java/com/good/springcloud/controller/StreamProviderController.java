package com.good.springcloud.controller;

import com.good.springcloud.service.StreamProviderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 15:09
 */
@RestController
public class StreamProviderController {

    @Resource
    private StreamProviderService providerService;

    @GetMapping("/send")
    public String send() {
        return providerService.send();
    }


}
