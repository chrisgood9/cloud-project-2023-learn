package com.good.springcloud.service;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 15:02
 */

public interface StreamProviderService {

    public String send();

}
