package com.good.springcloud.service.impl;

import com.good.springcloud.service.StreamProviderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/14 15:03
 */

/**
 * 和以往的架构不同，
 * 这里抽象的是一个 Output channel
 *
 * @EnableBinding :
 * 定义消息的推送管道 ,不是和controller打交道的service,而是发送消息的推送服务类
 */
@EnableBinding(Source.class)
@Slf4j
public class StreamProviderServiceImpl implements StreamProviderService {

    @Resource
    private MessageChannel output;  //消息发送管道

    @Override
    public String send() {
        String uuid = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(uuid).build());
        log.info("**** uuid = {}", uuid);
        return null;
    }
}
