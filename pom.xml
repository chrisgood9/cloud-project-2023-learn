<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!--  maven坐标  -->
    <groupId>com.good</groupId>
    <artifactId>cloud-project-2023-learn</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!--  子模块  -->
    <modules>
        <module>cloud-provider-payment-8001</module>
        <module>cloud-consumer-order-8080</module>
        <module>cloud-api-common</module>
        <module>cloud-eureka-service-7001</module>
        <module>cloud-eureka-service-7002</module>
        <module>cloud-provider-payment-8002</module>
        <module>cloud-provider-payment-8003</module>
        <module>cloud-consumer-zk-order-8080</module>
        <module>cloud-consumer-openfeign-order-8080</module>
        <module>cloud-consumer-hystrix-order-8080</module>
        <module>cloud-hystrix-payment-8005</module>
        <module>cloud-consumer-hystrix-dashboard-9001</module>
        <module>cloud-gateway-service-9527</module>
        <module>cloud-config-center-3344</module>
        <module>cloud-config-client-3355</module>
        <module>cloud-config-client-3366</module>
        <module>cloud-stream-provider-8001</module>
        <module>cloud-stream-consumer-8801</module>
        <module>cloud-stream-consumer-8802</module>
        <module>cloud-provider-consul-payment-8004</module>
        <module>cloud-consumer-consul-order-8080</module>
    </modules>

    <!--  打包方式：pom 总父工程 -->
    <packaging>pom</packaging>


    <!--  统一管理依赖版本 (jar包版本)  -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <junit.version>4.12</junit.version>
        <log4j.version>1.2.17</log4j.version>
        <lombok.version>1.16.18</lombok.version>
        <druid.version>1.1.16</druid.version>
        <mysql.version>8.0.30</mysql.version>
        <springfox.swagger.ui.version>2.9.2</springfox.swagger.ui.version>
        <springfox.swagger2.version>2.9.2</springfox.swagger2.version>
        <mybatis.spring.boot.version>1.3.0</mybatis.spring.boot.version>
        <spring.webmvc.version>4.2.8.RELEASE</spring.webmvc.version>
        <knife4j.version>2.0.6</knife4j.version>
        <eureka.server.version>2.2.1.RELEASE</eureka.server.version>
        <eureka.client.version>2.2.1.RELEASE</eureka.client.version>
    </properties>


    <!--  子模块继承之后: 锁定版本+子module不用写groupId和version  -->
    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>3.0.0</version>
            </dependency>

            <!-- 下面三个基本是微服务架构的标配 -->
            <!--spring boot 2.2.2-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.2.2.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--spring cloud Hoxton.SR1-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Hoxton.SR1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--spring cloud 阿里巴巴-->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>2.1.0.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!--mysql-->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql.version}</version>
            </dependency>

            <!-- druid-->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis.spring.boot.version}</version>
            </dependency>
            <!--junit-->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <!--log4j-->
            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>${log4j.version}</version>
            </dependency>

<!--            &lt;!&ndash; spring mvc依赖 &ndash;&gt;-->
<!--            <dependency>-->
<!--                <groupId>org.springframework</groupId>-->
<!--                <artifactId>spring-webmvc</artifactId>-->
<!--                <version>${spring.webmvc.version}</version>-->
<!--            </dependency>-->

            <!-- swagger-ui Knife4j，只需要导入这一个依赖就好了，无需在添加spring-fox的依赖 -->
            <dependency>
                <groupId>com.github.xiaoymin</groupId>
                <artifactId>knife4j-spring-boot-starter</artifactId>
                <version>${knife4j.version}</version>
            </dependency>

            <!--            &lt;!&ndash; swagger2核心依赖 &ndash;&gt;-->
<!--            <dependency>-->
<!--                <groupId>io.springfox</groupId>-->
<!--                <artifactId>springfox-swagger2</artifactId>-->
<!--                <version>${springfox.swagger2.version}</version>-->
<!--            </dependency>-->
<!--            <dependency>-->
<!--                <groupId>io.springfox</groupId>-->
<!--                <artifactId>springfox-swagger-ui</artifactId>-->
<!--                <version>${springfox.swagger.ui.version}</version>-->
<!--            </dependency>-->

            <!--eureka-server-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
                <version>${eureka.server.version}</version>
            </dependency>
            <!--eureka-client-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
                <version>${eureka.client.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.2.2.RELEASE</version>
                <configuration>
                    <fork>true</fork>
                    <addResources>true</addResources>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
